package com.yu.admin.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yu.admin.modules.system.pojo.SysUser;
import com.yu.admin.modules.system.pojo.dto.SysUserDTO;
import com.yu.admin.modules.system.pojo.vo.SysUserVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface SysUserMapper extends BaseMapper<SysUser> {

    @Select("select sys_user.id, sys_user.`username`, sys_user.`status`, sys_user.`status` ,\n" +
            " sys_role.`id` as roleId, sys_role.`name` as roleName from sys_user  \n" +
            " left join sys_role  on  sys_user.`role_id` = sys_role.`id`; ")
    List<SysUserVo> getAllUserVOs();

}
