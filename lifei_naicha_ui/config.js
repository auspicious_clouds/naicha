const configDev = {
  baseUrl: "http://localhost:9002",
  env: "dev", 
  debug: true
}

const configProd = {
  baseUrl: 'https://yuqy.club:9001',
  env: "prod", 
  debug: false
}

// export default configDev
export default configProd
