#基于uniapp的奶茶店点餐微信小程序+基于SpringBoot和Vue的管理后台

### 小程序截图
![小程序截图.png](截图/小程序截图.png)
#### 润土刺茶
![](截图/scan.jpg)


### 管理后台截图
![小程序截图.png](截图/闰土刺茶管理后台截图.png)




感谢：基于uni-app开发的仿奈雪の茶小程序前端模板，让我免去小程序的界面设计与开发
https://gitee.com/tinypuppet/nxdc-milktea